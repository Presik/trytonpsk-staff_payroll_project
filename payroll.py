# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import PoolMeta, Pool
from decimal import Decimal
from trytond.model import ModelView, fields
from trytond.wizard import Wizard, StateView, Button, StateReport
from trytond.report import Report
from trytond.transaction import Transaction


class Payroll(metaclass=PoolMeta):
    __name__ = 'staff.payroll'
    project = fields.Many2One('project.work', 'Project', domain=[
        ('type', '=', 'project')
    ])

    @classmethod
    def __setup__(cls):
        super(Payroll, cls).__setup__()

    # @fields.depends('contract')
    # def on_change_with_project(self, name=None):
    #     if self.contract and self.contract.project:
    #         return self.contract.project.id

    @fields.depends('period', 'employee', 'start', 'end', 'contract',
        'description', 'date_effective', 'last_payroll', 'project')
    def on_change_period(self):
        super(Payroll, self).on_change_period()
        if self.contract and self.contract.project:
            self.project = self.contract.project.id


class PayrollGroup(metaclass=PoolMeta):
    'Payroll Group'
    __name__ = 'staff.payroll_group'

    def get_values(self, contract, start_date, end_date):
        values = super(PayrollGroup, self).get_values(contract, start_date, end_date)
        values['project'] = contract.project.id if contract.project else None
        return values


class PayrollPaycheckStart(metaclass=PoolMeta):
    __name__ = 'staff.payroll_paycheck.start'
    project = fields.Many2One('project.work', 'Project', domain=[
        ('type', '=', 'project'),
        ('parent', '!=', None)
    ])


class PayrollPaycheck(metaclass=PoolMeta):
    __name__ = 'staff.payroll.paycheck'

    def do_print_(self, action):
        action, data = super(PayrollPaycheck, self).do_print_(action)
        project_id = None
        if self.start.project:
            project_id = self.start.project.id
        data['project'] = project_id,
        return action, data


class PayrollPaycheckReport(metaclass=PoolMeta):
    __name__ = 'staff.payroll.paycheck_report'

    @classmethod
    def get_domain_payroll(cls, data):
        dom_payroll = super(PayrollPaycheckReport, cls).get_domain_payroll(data)
        if data.get('project') and data['project'][0]:
            dom_payroll.append(
                ('project', '=', data['project'][0])
            )
        return dom_payroll


class PayrollSheetStart(metaclass=PoolMeta):
    __name__ = 'staff.payroll.sheet.start'
    project = fields.Many2One('project.work', 'Project', domain=[
        ('type', '=', 'project')
    ])


class PayrollSheet(metaclass=PoolMeta):
    __name__ = 'staff.payroll.sheet'

    def do_print_(self, action):
        action, data = super(PayrollSheet, self).do_print_(action)
        project_id = None
        if self.start.project:
            project_id = self.start.project.id
        data['project'] = project_id
        return action, data


class PayrollSheetConvencionalStart(ModelView):
    'Payroll Sheet Convencional Start'
    __name__ = 'staff.payroll.sheet.convencional.start'
    # periods = fields.One2Many('staff.payroll.period', None,
    #     'Periods', add_remove=[], required=True)
    company = fields.Many2One('company.company', 'Company', required=True)
    start_period = fields.Many2One('staff.payroll.period', 'Start Period', required=True)
    end_period = fields.Many2One('staff.payroll.period', 'End Period', required=True)
    project = fields.Many2One('project.work', 'Project', domain=[
        ('type', '=', 'project')
    ])
    group = fields.Boolean('Group')

    @staticmethod
    def default_company():
        return Transaction().context.get('company')


class PayrollSheetConvencional(Wizard):
    'Payroll Sheet Convencional'
    __name__ = 'staff.payroll.sheet.convencional'
    start = StateView('staff.payroll.sheet.convencional.start',
        'staff_payroll_project.payroll_sheet_convencional_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Print', 'print_', 'tryton-ok', default=True),
            ])
    print_ = StateReport('staff.payroll.sheet_convencional_report')

    def do_print_(self, action):
        # periods = [p.id for p in self.start.periods]
        project_id = self.start.project.id \
            if self.start.project else None
        data = {
            'company': self.start.company.id,
            # 'periods': periods,
            'start_period': self.start.start_period.id,
            'end_period': self.start.end_period.id,
            'project': project_id,
            'group': self.start.group,
        }
        return action, data

    def transition_print_(self):
        return 'end'


class PayrollSheetConvencionalReport(Report):
    __name__ = 'staff.payroll.sheet_convencional_report'

    @classmethod
    def get_domain_payroll(cls, data):
        dom_payroll = []

        return dom_payroll

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        pool = Pool()
        user = pool.get('res.user')(Transaction().user)
        Payroll = pool.get('staff.payroll')
        PayrollLine = pool.get('staff.payroll.line')
        cache_wage_dict = Payroll.create_cache_wage_types()
        Period = pool.get('staff.payroll.period')
        clause = []
        dom_payroll = cls.get_domain_payroll(data)
        start_period, = Period.search([
            ('id', '=', data['start_period'])
        ])
        end_period, = Period.search([
            ('id', '=', data['end_period'])
        ])
        dom_payroll.append([
            ('period.start', '>=', start_period.start),
            ('period.end', '<=', end_period.end)
        ])
        if data['project'] not in (None, ''):
            dom_payroll.append([
                ('contract.project', '=', data['project'])
            ])

        payrolls = Payroll.search(dom_payroll,  order=[('employee.party.name', 'ASC'), ('period.name', 'ASC')])

        # new_objects = []
        # default_vals = cls.default_values()
        # sum_gross_payments = []
        # sum_total_deductions = []
        # sum_net_payment = []
        # item = 0

        if data['group']:
            results_payroll = cls.print_goup_by_contract(cls, payrolls, user, cache_wage_dict)
        else:
            results_payroll = cls.print_all_payrolls(cls, payrolls, user, cache_wage_dict)
        report_context['records'] = results_payroll['new_objects']
        report_context['start'] = start_period.name
        report_context['end'] = end_period.name
        report_context['company'] = user.company
        report_context['user'] = user
        report_context['sum_gross_payments'] = sum(results_payroll['sum_gross_payments'])
        report_context['sum_net_payment'] = sum(results_payroll['sum_net_payment'])
        report_context['sum_total_deductions'] = sum(results_payroll['sum_total_deductions'])
        return report_context

    @classmethod
    def default_values(cls):
        fields_no_amount = [
                'item',
                'employee',
                'id_number',
                'position',
                'legal_salary',
                'salary_day',
                'salary_hour',
                'worked_days',
                'period',
                'department',
        ]

        fields_amount = [
                'salary',
                'reco',
                'recf',
                'hedo',
                'heno',
                'dom',
                'hedf',
                'henf',
                'cost_reco',
                'cost_recf',
                'cost_hedo',
                'cost_heno',
                'cost_dom',
                'cost_hedf',
                'cost_henf',
                'bonus',
                'total_extras',
                'gross_payment',
                'health',
                'retirement',
                'food',
                'transport',
                'fsp',
                'syndicate',
                'retefuente',
                'other_deduction',
                'total_deduction',
                'ibc',
                'net_payment',
                'box_family',
                'box_family',
                'unemployment',
                'interest',
                'holidays',
                'bonus_service',
                'discount',
                'other',
                'total_benefit',
                'risk',
                'retirement_provision',
                'health_provision',
                'total_ssi',
                'total_cost',
                'sena',
                'icbf',
                'acquired_product',
                'convencional_bonus',
                'vacation_bonus',
                'room_bonus',
                'salary_readjustment',
                'non_salary_readjustment',
                'non_salary_bonus',
                'incapacity_greater_to_2_days',
                'incapacity_less_to_2_days',
                'incapacity_arl',
        ]

        if '' in fields_no_amount:
            fields_no_amount.remove('')

        default_values = {}
        for field in fields_no_amount:
            default_values.setdefault(field, None)
        for field in fields_amount:
            default_values.setdefault(field, Decimal(0))
        return default_values

    @classmethod
    def _prepare_lines(cls, payroll, vals, cache_wage_dict):
        extras =[
                'reco',
                'recf',
                'hedo',
                'heno',
                'dom',
                'hedf',
                'henf',
        ]
        for line in payroll.lines:
            wage_id = line.wage_type.id
            wage = cache_wage_dict[wage_id]
            definition = wage['definition']
            type_concept = wage['type_concept']
            if definition == 'payment':
                if type_concept == 'salary':
                    vals['salary'] += line.amount
                elif type_concept == 'extras':
                    vals['total_extras'] += line.amount
                    for e in extras:
                        if e.upper() in wage['name']:
                            vals[e] += line.quantity or 0
                            vals['cost_' + e] += line.amount
                            break
                elif type_concept == 'risk':
                    vals['risk'] += line.amount
                elif type_concept == 'box_family':
                    vals['box_family'] += line.amount
                elif type_concept == 'unemployment':
                    vals['unemployment'] += line.amount
                elif type_concept == 'interest':
                    vals['interest'] += line.amount
                elif type_concept == 'holidays':
                    vals['holidays'] += line.amount
                elif type_concept == 'bonus':
                    vals['bonus'] += line.amount
                elif type_concept == 'bonus_service':
                    vals['bonus_service'] += line.amount
                elif type_concept == 'transport':
                    vals['transport'] += line.amount
                elif type_concept == 'food':
                    vals['food'] += line.amount
                elif type_concept == 'sena':
                    vals['sena'] += line.amount
                elif type_concept == 'icbf':
                    vals['icbf'] += line.amount
                    'non_salary_readjustment',
                elif type_concept == 'convencional_bonus':
                    vals['convencional_bonus'] += line.amount
                elif type_concept == 'vacation_bonus':
                    vals['vacation_bonus'] += line.amount
                elif type_concept == 'room_bonus':
                    vals['room_bonus'] += line.amount
                elif type_concept == 'salary_readjustment':
                    vals['salary_readjustment'] += line.amount
                elif type_concept == 'non_salary_readjustment':
                    vals['non_salary_readjustment'] += line.amount
                elif type_concept == 'non_salary_bonus':
                    vals['non_salary_bonus'] += line.amount
                elif type_concept == 'incapacity_greater_to_2_days':
                    vals['incapacity_greater_to_2_days'] += line.amount
                elif type_concept == 'incapacity_less_to_2_days':
                    vals['incapacity_less_to_2_days'] += line.amount
                elif type_concept == 'incapacity_arl':
                    vals['incapacity_arl'] += line.amount
                else:
                    print('Warning: Line no processed... ', wage['name'])
                    vals['other'] += line.amount
            elif definition == 'deduction':
                vals['total_deduction'] += line.amount
                if type_concept == 'health':
                    vals['health'] += line.amount
                    if wage['expense_formula']:
                        vals['health_provision'] += line.get_expense_amount(wage)
                elif type_concept == 'retirement':
                    vals['retirement'] += line.amount
                    if wage['expense_formula']:
                        vals['retirement_provision'] += line.get_expense_amount(wage)
                else:
                    if type_concept == 'fsp':
                        vals['fsp'] += line.amount
                    elif type_concept == 'tax':
                        vals['retefuente'] += line.amount
                    elif type_concept == 'syndicate':
                        vals['syndicate'] += line.amount
                    else:
                        vals['other_deduction'] += line.amount
            else:
                if type_concept == 'acquired_product':
                    vals['acquired_product'] += line.amount
                vals['discount'] += line.amount
                print('Warning: Line no processed... ', wage['name'])

        vals['gross_payment'] = (
            vals['salary'] + vals['total_extras'] +
            vals['transport'] + vals['food'] + vals['bonus'] +
            vals['convencional_bonus'] + vals['vacation_bonus'] +
            vals['room_bonus'] + vals['salary_readjustment'] +
            vals['non_salary_readjustment'] + vals['non_salary_bonus'] +
            vals['incapacity_greater_to_2_days'] +
            vals['incapacity_less_to_2_days'] +
            vals['incapacity_arl'] + vals['other']
            )
        vals['net_payment'] = vals['gross_payment'] - vals['total_deduction']-vals['discount']
        vals['total_incapacity'] = (
            vals['incapacity_greater_to_2_days'] +
            vals['incapacity_less_to_2_days'] + vals['incapacity_arl'] +
            vals['incapacity_less_to_2_days'] + vals['other']
            )
        vals['ibc'] = vals['gross_payment'] - vals['transport']
        vals['total_benefit'] = vals['unemployment'] + vals['interest']  + vals['holidays'] + vals['bonus_service']
        vals['total_ssi'] = vals['retirement_provision'] + vals['risk'] + vals['health_provision']
        vals['total_contributions'] = (
            vals['total_ssi'] + vals['box_family'] +
            vals['sena'] + vals['icbf'] + vals['total_benefit']
            )
        vals['total_cost'] = (
            vals['total_contributions'] + vals['gross_payment']
            )
        return vals

    def print_all_payrolls(cls, payrolls, user, cache_wage_dict):
        results_print_payrolls = {}
        new_objects = []
        default_vals = cls.default_values()
        sum_gross_payments = []
        sum_total_deductions = []
        sum_net_payment = []
        item = 0
        for payroll in payrolls:
            item += 1
            values = default_vals.copy()
            values['item'] = item
            values['employee'] = payroll.employee.party.name
            values['id_number'] = payroll.employee.party.id_number
            position_name, position_contract = '', ''
            if payroll.employee.position:
                position_name = payroll.employee.position.name
            if payroll.contract and payroll.contract.position:
                position_contract = payroll.contract.position.name
            values['position'] = position_contract or position_name
            values['department'] = payroll.employee.department.name \
                if payroll.employee.department else ''
            values['company'] = user.company.party.name
            values['legal_salary'] = payroll.contract.get_salary_in_date(
                payroll.end)
            values['period'] = payroll.period.name
            salary_day_in_date = payroll.contract.get_salary_in_date(
                payroll.end)/30
            values['salary_day'] = salary_day_in_date
            values['salary_hour'] = (salary_day_in_date / 8) if salary_day_in_date else 0
            values['worked_days'] = payroll.worked_days
            values['gross_payment'] = payroll.gross_payments
            # Add compatibility with staff contracting
            project = ""
            if hasattr(payroll, 'project'):
                if payroll.project:
                    project = payroll.project.name

            if hasattr(payroll.employee, 'project_contract'):
                if payroll.employee.project_contract and \
                    payroll.employee.project_contract.reference:
                    project = payroll.employee.project_contract.reference
            values['project'] = project

            values.update(cls._prepare_lines(payroll, values, cache_wage_dict))
            sum_gross_payments.append(payroll.gross_payments)
            sum_total_deductions.append(payroll.total_deductions)
            sum_net_payment.append(payroll.net_payment)
            new_objects.append(values)
        results_print_payrolls['sum_gross_payments'] = sum_gross_payments
        results_print_payrolls['sum_total_deductions'] = sum_total_deductions
        results_print_payrolls['sum_net_payment'] = sum_net_payment
        results_print_payrolls['new_objects'] = new_objects
        return results_print_payrolls

    def print_goup_by_contract(cls, payrolls, user, cache_wage_dict):
        results_print_payrolls = {}
        new_objects = []
        default_vals = cls.default_values()
        sum_gross_payments = []
        sum_total_deductions = []
        sum_net_payment = []
        item = 0
        payroll_dict = {}
        for payroll in payrolls:
            item += 1
            if payroll.contract.id not in payroll_dict:
                values = default_vals.copy()
                values['item'] = item
                values['employee'] = payroll.employee.party.name
                values['id_number'] = payroll.employee.party.id_number
                position_name, position_contract = '', ''
                if payroll.employee.position:
                    position_name = payroll.employee.position.name
                if payroll.contract and payroll.contract.position:
                    position_contract = payroll.contract.position.name
                values['position'] = position_contract or position_name
                values['department'] = payroll.employee.department.name \
                    if payroll.employee.department else ''
                values['company'] = user.company.party.name
                values['legal_salary'] = payroll.contract.get_salary_in_date(
                    payroll.end)
                values['period'] = payroll.period.name
                salary_day_in_date = payroll.contract.get_salary_in_date(
                    payroll.end)/30
                values['salary_day'] = salary_day_in_date
                values['salary_hour'] = (salary_day_in_date / 8) if salary_day_in_date else 0
                values['worked_days'] = payroll.worked_days
                values['gross_payment'] = payroll.gross_payments
                # Add compatibility with staff contracting
                project = ""
                if hasattr(payroll, 'project'):
                    if payroll.project:
                        project = payroll.project.name

                if hasattr(payroll.employee, 'project_contract'):
                    if payroll.employee.project_contract and \
                        payroll.employee.project_contract.reference:
                        project = payroll.employee.project_contract.reference
                values['project'] = project
                values.update(cls._prepare_lines(payroll, values, cache_wage_dict))
                payroll_dict[payroll.contract.id] = values
            else:
                payroll_dict[payroll.contract.id]['worked_days'] += payroll.worked_days
                payroll_dict[payroll.contract.id]['gross_payment'] += payroll.gross_payments
                payroll_dict[payroll.contract.id].update(cls._prepare_lines(payroll, payroll_dict[payroll.contract.id], cache_wage_dict))
            sum_gross_payments.append(payroll.gross_payments)
            sum_total_deductions.append(payroll.total_deductions)
            sum_net_payment.append(payroll.net_payment)
            new_objects.append(values)
        results_print_payrolls['sum_gross_payments'] = sum_gross_payments
        results_print_payrolls['sum_total_deductions'] = sum_total_deductions
        results_print_payrolls['sum_net_payment'] = sum_net_payment
        results_print_payrolls['new_objects'] = payroll_dict.values()
        return results_print_payrolls


class PayrollGlobalStart(metaclass=PoolMeta):
    __name__ = 'staff.payroll_global.start'
    project = fields.Many2One('project.work', 'Project', domain=[
        ('type', '=', 'project')
        ])


class PayrollGlobal(metaclass=PoolMeta):
    __name__ = 'staff.payroll.global'

    def do_print_(self, action):
        action, data = super(PayrollGlobal, self).do_print_(action)
        project_id = None
        if self.start.project:
            project_id = self.start.project.id
        data['project'] = project_id,
        return action, data


class PayrollGlobalReport(metaclass=PoolMeta):
    __name__ = 'staff.payroll.global_report'

    @classmethod
    def get_domain_payroll(cls, data):
        # dom_payroll = super(PayrollSheetReport, cls).get_domain_payroll(data)
        dom_payroll = []
        if data['project'][0]:
            dom_payroll.append(
                ('project', 'in', data['project'])
            )
        return dom_payroll
    
